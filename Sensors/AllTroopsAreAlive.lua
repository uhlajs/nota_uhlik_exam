local sensorInfo = {
  name = "AllTroopsAreAlive",
  desc = "Chech whether all troops in given array are alive.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local ValidUnitID = Spring.ValidUnitID

--- Chech whether all troops in given array are alive.
-- @param listOfUnits [unitID]: list of units
return function(listOfUnits)
  if listOfUnits == nil or #listOfUnits == 0 then
    return false
  end
	
	for _, unitID in ipairs(listOfUnits) do
		local isAlive = ValidUnitID(unitID)
		if not isAlive then
			return false
		end
	end
	return true
end