local sensorInfo = {
	name = "GetTroopsCountByLine",
	desc = "Get count of all troops grouped by it's group.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local QueryTroops = Sensors.QueryTroops
local GetUnitDefID = Spring.GetUnitDefID

local function getDefsName(unitID)
  return UnitDefs[GetUnitDefID(unitID)].name
end

local function initCounts(group)
  local troopsCountByDefsName = {}
  for key, _ in pairs(bb.GroupsCountSchedule[group]) do
    troopsCountByDefsName[key] = 0
  end
  return troopsCountByDefsName
end

local function getTroopsCountByDefsName(group)
  local troopsCountByDefsName = initCounts(group)
  local troops = QueryTroops({group = group})
  for _, unitID in ipairs(troops) do
    local defsName = getDefsName(unitID)
    if troopsCountByDefsName[defsName] then
      troopsCountByDefsName[defsName] = troopsCountByDefsName[defsName] + 1
    else
      troopsCountByDefsName[defsName] = 0
    end
  end
  return troopsCountByDefsName
end


--- Get count of all troops grouped by it's group.
--- It's mainly intended for TroopsManager in order to know how which and many units it should buy.
--- Global bb variables:
--   `bb.GroupsCountSchedule` from `GenerateTroopsInfo.lua`
--   `bb.GroupsNames` from `GenerateTroopsInfo.lua`
return function()
  local res = {}
  for _, group in ipairs(bb.GroupsNames) do
    res[group] = getTroopsCountByDefsName(group)
  end
  return res
end