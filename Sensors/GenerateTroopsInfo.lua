local sensorInfo = {
	name = "GenerateTroopsInfo",
	desc = "Generate metadata about all user units.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- cache

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local OFFSET_DEBUG = 0
-- Default line offsets
bb.UnitsOffsets = {
  armfark = -1 + OFFSET_DEBUG,
  armpw = -1 + OFFSET_DEBUG,
  armzeus = 0 + OFFSET_DEBUG,
  armatlas = -1 + OFFSET_DEBUG,
  armmav = -1 + OFFSET_DEBUG,
  armmart = -1 + OFFSET_DEBUG,
  armseer = -3 + OFFSET_DEBUG,
  armbox = -1 + OFFSET_DEBUG,
  armatlas = -2 + OFFSET_DEBUG,
}

-- Goal to assing to each group
bb.GroupsNames = {"mid", "top", "bot"}
bb.GroupsCountSchedule = {
  mid = {
    armfark = 2,
    armatlas = 4,
    armbox = 4,
    armseer = 1,
    armmart = 10,
    armmav = 2,
    armzeus = 5,
  },
  top = {
   --[[  armfark = 1,
    armatlas = 1,
    armbox = 1, ]]
  },
  bot = {
 --[[    armfark = 1,
    armatlas = 1,
    armbox = 1, ]]
  },
}

bb.TroopsCounter = 0

local ValidUnitID = Spring.ValidUnitID
local GetMyTeamID = Spring.GetMyTeamID
local GetTeamUnits = Spring.GetTeamUnits
local GetUnitDefID = Spring.GetUnitDefID
local GetTroopsCountByGroups = Sensors.GetTroopsCountByGroups

local function getDefsName(unitID)
  return UnitDefs[GetUnitDefID(unitID)].name
end

local function getGroupForUnit(unitDefsName, troopsCountByGroups)
  for _, group in ipairs(bb.GroupsNames) do
    local troopsCount = troopsCountByGroups[group]
    if not bb.GroupsCountSchedule[group][unitDefsName] or bb.GroupsCountSchedule[group][unitDefsName] == 0 then
      do end
    elseif not troopsCount[unitDefsName] then
      troopsCount[unitDefsName] = 1
      return group
    elseif troopsCount[unitDefsName] < bb.GroupsCountSchedule[group][unitDefsName] then
      troopsCount[unitDefsName] = troopsCount[unitDefsName] + 1
      return group
    end
  end
  return "none"
end

local function addNewUnits()
  local teamUnits = GetTeamUnits(GetMyTeamID())
  local troopsCountByGroups = GetTroopsCountByGroups() 

  for _, unitID in ipairs(teamUnits) do
    if not bb.troopsInfoMap[unitID] then
      bb.TroopsCounter = bb.TroopsCounter + 1
      local unitDefsName = getDefsName(unitID)
        bb.troopsInfoMap[unitID] = {
          status = "idle",
          group = getGroupForUnit(unitDefsName, troopsCountByGroups),
          defsName = unitDefsName,
          id = bb.TroopsCounter
        }
    end
  end
end

local function assignIdleUnits()
  local troopsCountByGroups = GetTroopsCountByGroups() 

  for unitID, _ in pairs(bb.troopsInfoMap) do
    local info = bb.troopsInfoMap[unitID]
    if info.group == "none" and info.status == "idle" then
      info.group = getGroupForUnit(info.defsName, troopsCountByGroups)
    end
  end
end

local function removeDeadUnits()
  for unitID, _ in pairs(bb.troopsInfoMap) do
    if not ValidUnitID(unitID) then
      bb.troopsInfoMap[unitID] = nil
    end
  end
end

--- Create bb map mapping unit to its meta-information record.
--- It's repeatedly updating these records and removing records of dead units.
return function()
  bb.troopsInfoMap = bb.troopsInfoMap or {}

  removeDeadUnits()
  addNewUnits()
  assignIdleUnits()

  return bb.troopsInfoMap
end