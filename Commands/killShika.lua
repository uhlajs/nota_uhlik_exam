function getInfo()
  return {
      onNoUnits = SUCCESS,
      tooltip = "",
      parameterDefs = {
          {
              name = "troops",
              variableType = "expression",
              componentType = "editBox",
              defaultValue = "troops",
          },
          {
            name = "enemyTeamIDs",
            variableType = "expression",
            componentType = "editBox",
            defaultValue = "core.EnemyTeamIDs()",
        },
      }
  }
end

local function ClearState(self)
  self.initialization = false
end

local GetUnitPosition = Spring.GetUnitPosition
local GetUnitsInSphere = Spring.GetUnitsInSphere
local ValidUnitID = Spring.ValidUnitID
local GetUnitDefID = Spring.GetUnitDefID

local function distance(a, b)
	return math.sqrt(math.pow(a.x - b.x, 2) + math.pow(a.z - b.z, 2))
end

local function checkFail(self, parameter)
  return false
end

local function checkSuccess(self, parameter)
  return false
end

local function getDefsName(unitID)
  local index = GetUnitDefID(unitID)
  if index ~= nil then
    return UnitDefs[index].name
  end
end

local function getShikaID(self, parameter)
  for _,unitID in ipairs(parameter.troops) do
    local x, y, z = GetUnitPosition(unitID)
    local center = Vec3(x, y, z)
    local radius = 1350 -- range
    local enemyIDs = parameter.enemyTeamIDs

    for _, enemyTeamID in ipairs(enemyIDs) do
      local enemyUnits = GetUnitsInSphere(center.x, center.y, center.z, radius, enemyTeamID)
      for _, enemyID in ipairs(enemyUnits) do
        if getDefsName(enemyID) == "shika" then
          return enemyID
        end
      end 
    end
  end
  return nil
end

function Run(self, units, parameter)
  if checkFail(self, parameter) then
      return FAILURE
  elseif checkSuccess(self, parameter) then
      return SUCCESS
  end

  local shikaID = getShikaID(self, parameter)
  if shikaID == nil then
    return SUCCESS
  end
  
  if not self.initialization then
    self.initialization = true
    for _, unitID in ipairs(parameter.troops) do
      Spring.GiveOrderToUnit(
          unitID,
          CMD.FIRE_STATE,
          {0},
          {}
      )
      Spring.GiveOrderToUnit(
          unitID,
          CMD.ATTACK,
          {shikaID},
          {}
      )
    end
  end

  return RUNNING
end

function Reset(self)
  ClearState(self)
end