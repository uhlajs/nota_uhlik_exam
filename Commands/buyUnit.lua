-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Buy unit in param",
        parameterDefs = {
            { 
                name = "defsName",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "'armbox'",
            },
        }
    }
end

local myTeamID = Spring.GetMyTeamID()
local GetUnitDefID = Spring.GetUnitDefID

local function hasEnoughMetal(defsName)
    -- TODO: check if have enought metal
    local resources = Spring.GetTeamResources(myTeamID, "metal")
    local cost = UnitDefNames[defsName].metalCost
    local flag = resources >= cost + 5

    if flag then
        Spring.Echo("Buying: " .. defsName .. " for " .. cost .. "/" .. resources)
    else
        Spring.Echo("No enought metal for: " .. defsName .. " missing " .. cost - resources)
    end

    return flag
end

local function removeFromOrderList(defsName)
    local topOrder = bb.OrderList[1]

    -- Decrease count
    if topOrder ~= nil and topOrder.defsName == defsName then
        topOrder.count = topOrder.count - 1

        -- Remove from order list
        if topOrder.count == 0 then
            table.remove(bb.OrderList, 1)
        end
    end
end

function Run(self, units, parameter)
    if not hasEnoughMetal(parameter.defsName) then
        return FAILURE
    end

    removeFromOrderList(parameter.defsName)

    message.SendRules({
        subject = "swampdota_buyUnit",
        data = {
            unitName = parameter.defsName
        },
    })

    return SUCCESS
end


function Reset(self)
    return self
end