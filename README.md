# NOTA

Author: **Jan Uhlík (uhlajs)**

## Credits

All icons are taken from [Icons8](https://icons8.com) and are distributed under a [Creative Commons Attribution-NoDerivs 3.0 Unported](https://creativecommons.org/licenses/by-nd/3.0/) license.