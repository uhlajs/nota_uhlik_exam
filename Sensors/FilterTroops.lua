local sensorInfo = {
	name = "FilterTroops",
	desc = "Filter units according given filter.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local ValidUnitID = Spring.ValidUnitID
local GetMyTeamID = Spring.GetMyTeamID
local GetTeamUnits = Spring.GetTeamUnits
local GetUnitDefID = Spring.GetUnitDefID

local function isInArray(element, array)
  if type(array) ~= "table" then return false end
  for _, e in ipairs(array) do
      if e == element then
          return true
      end
  end
  return false
end

--- Filter units according given filter.
--- Global bb variables:
---   `bb.troopsInfoMap` from `GenerateTroopsInfo.lua`
-- @param filter table: filter in format
-- filter = {
--  status = unit status
--  group = group name
--  defsName = type of the unit
-- } 
-- @return [unitID]: array of filtered units.
return function(troops, filter)
  local newTroops = {}
  for _, unitID in ipairs(troops) do
    local unitInfo = bb.troopsInfoMap[unitID]
    local valid = true
    for key, value in pairs(filter) do
      if value ~= "*" and value ~= unitInfo[key] and not isInArray(value, unitInfo[key]) then
        valid = false
        break
      end
    end

    if valid then
      newTroops[#newTroops + 1] = unitID
    end
  end
  return newTroops
end