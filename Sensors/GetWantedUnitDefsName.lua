local sensorInfo = {
	name = "GetWantedUnitDefsName",
	desc = "Return name of next wanted unit to buy.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- Top wanted units
bb.OrderList = {
   {
    defsName = "armfark",
    count = 2,
  },
  {
    defsName = "armseer",
    count = 1,
  },
  {
    defsName = "armmart",
    count = 3,
  },
--[[   {
    defsName = "armzeus",
    count = 5,
  },
  {
    defsName = "lineUpgrade",
    count=5, 
    line="GoodMiddle"
  }, ]]
}
bb.UnitsPriorities = {"armfark", "armseer", "armatlas", "armbox", "armmav", "armzeus", "armmart"}

local GetTroopsCountByGroups = Sensors.GetTroopsCountByGroups

local function isIn(element, array)
  for _, e in ipairs(array) do
      if e == element then
          return true
      end
  end
  return false
end

local function supplyGroups()
  local troopsCountByGroups = GetTroopsCountByGroups()
  for _, unitDefsName in ipairs(bb.UnitsPriorities) do
    for _, groupName in ipairs(bb.GroupsNames) do
      local groupSchedule = bb.GroupsCountSchedule[groupName]
      local count = groupSchedule[unitDefsName]
      if count ~= nil and count ~= 0 then
        if troopsCountByGroups[groupName][unitDefsName] == nil or
           troopsCountByGroups[groupName][unitDefsName] < count then
           return unitDefsName
        end
      end
    end
  end
end


--- Return name of next wanted unit to buy.
--- Global bb variables:
--   `bb.GroupsCountSchedule` from `GenerateTroopsInfo.lua`
return function()
  -- Firstly buy order list
  for _, order in ipairs(bb.OrderList) do
    return order
  end

  -- If there is no order in order list -> supplyGroups
  local candidate = supplyGroups()
  if candidate ~= nil then
    return {defsName = candidate, count=1}
  end

  -- Just update line
  return {defsName = "lineUpgrade", count=5, line="GoodMiddle"}
end