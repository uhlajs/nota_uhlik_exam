local sensorInfo = {
	name = "playground",
	desc = "",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local MissionInfo = Sensors.core.MissionInfo

return function()
  bb.missionInfo = MissionInfo()
end