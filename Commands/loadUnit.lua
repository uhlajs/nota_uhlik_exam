function getInfo()
    return {
        onNoUnits = SUCCESS,
        tooltip = "",
        parameterDefs = {
            {
                name = "transporterID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "transporterID",
            },
            {
                name = "passengerID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "passengerID",
            },
        }
    }
end

local ValidUnitID = Spring.ValidUnitID
local GetUnitIsTransporting = Spring.GetUnitIsTransporting
local GiveOrderToUnit = Spring.GiveOrderToUnit
local GetUnitTransporter = Spring.GetUnitTransporter

local function ClearState(self)
    self.initialization = false
end

local function checkFail(self, parameter)
    return (not self.initialization and #GetUnitIsTransporting(parameter.transporterID) ~= 0 and GetUnitIsTransporting(parameter.transporterID)[1] ~= parameter.passengerID)  -- Transporter already loaded at the beginning
            or
            (GetUnitTransporter(parameter.passengerID) ~= nil and GetUnitTransporter(parameter.passengerID) ~= parameter.transporterID) -- Passanger already loaded by different transporter
            or
            not ValidUnitID(parameter.transporterID) -- trasporter was destroyed
end

local function checkSuccess(self, parameter)
    return GetUnitIsTransporting(parameter.transporterID)[1] == parameter.passengerID -- Alreade unloaded
end

function Run(self, units, parameter)
    if checkFail(self, parameter) then
        return FAILURE
    elseif checkSuccess(self, parameter) then
        return SUCCESS
    end

    if not self.initialization then
        self.initialization = true
        GiveOrderToUnit(
            parameter.transporterID,
            CMD.LOAD_UNITS,
            {parameter.passengerID}, --{x,y,z,250},
            {} -- {"shift"}
        )
    end
    return RUNNING
end

function Reset(self)
    ClearState(self)
end