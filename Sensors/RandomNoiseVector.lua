local sensorInfo = {
	name = "RandomNoiseVector",
	desc = "Random noise vector.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local function randomSign()
  local sign
  if math.random() < 0.5 then
    sign = -1
  else
    sign = 1
  end
  return sign
end

return function(m, n, alsoY)
  local m = m or 50
  local n = n or 100
  local alsoY = alsoY or false
  if alsoY then
    return Vec3(randomSign() * math.random(m,n), randomSign() * math.random(m,n), randomSign() * math.random(m,n))
  else
    return Vec3(randomSign() * math.random(m,n), 0, randomSign() * math.random(m,n))
  end
end