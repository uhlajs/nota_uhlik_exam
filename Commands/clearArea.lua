function getInfo()
  return {
      onNoUnits = SUCCESS,
      tooltip = "",
      parameterDefs = {
          {
              name = "troops",
              variableType = "expression",
              componentType = "editBox",
              defaultValue = "troops",
          },
          {
            name = "center",
            variableType = "expression",
            componentType = "editBox",
            defaultValue = "center",
          },
          {
              name = "radius",
              variableType = "expression",
              componentType = "editBox",
              defaultValue = "radius",
          },
      }
  }
end

local GiveOrderToUnit = Spring.GiveOrderToUnit
local GetFeaturesInSphere = Spring.GetFeaturesInSphere

local function ClearState(self)
  self.initialization = false
end

local function checkFail(self, parameter)
  return 
end

local function checkSuccess(self, parameter)
  return #GetFeaturesInSphere(parameter.center.x, parameter.center.y, parameter.center.z, parameter.radius) == 0
end

function Run(self, units, parameter)
  if checkFail(self, parameter) then
      return FAILURE
  elseif checkSuccess(self, parameter) then
      return SUCCESS
  end

  if not self.initialization then
    self.initialization = true
    for _, unitID in ipairs(parameter.troops) do
      GiveOrderToUnit(
        unitID,
        CMD.RECLAIM,
        {parameter.center.x, parameter.center.y, parameter.center.z, parameter.radius}, --{x,y,z,250},
        {} -- {"shift"}
      )
    end
  end
  return RUNNING
end

function Reset(self)
  ClearState(self)
end