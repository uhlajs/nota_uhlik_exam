function getInfo()
    return {
        onNoUnits = SUCCESS,
        tooltip = "",
        parameterDefs = {
            {
                name = "transporterID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "transporterID",
            },
        }
    }
end

local ValidUnitID = Spring.ValidUnitID
local GetUnitIsTransporting = Spring.GetUnitIsTransporting
local GiveOrderToUnit = Spring.GiveOrderToUnit
local GetUnitPosition = Spring.GetUnitPosition

local function ClearState(self)
    self.initialization = false
end

local function checkFail(self, parameter)
    return (not self.initialization and #GetUnitIsTransporting(parameter.transporterID) == 0)  -- No units is loaded at the beginning
            or
            not ValidUnitID(parameter.transporterID) -- trasporter was destroyed
end

local function checkSuccess(self, parameter)
    return (self.initialization and #GetUnitIsTransporting(parameter.transporterID) == 0) -- Alreade unloaded
end

function Run(self, units, parameter)
    if checkFail(self, parameter) then
        return FAILURE
    elseif checkSuccess(self, parameter) then
        return SUCCESS
    end

    if not self.initialization then
        self.initialization = true
        local x, y, z = GetUnitPosition(parameter.transporterID)
        GiveOrderToUnit(
            parameter.transporterID,
            CMD.UNLOAD_UNIT,
            {x, y, z}, 
            --{parameter.passengerID}, --{x,y,z,250},
            {} -- {"shift"}
        )
    end
    return RUNNING
end

function Reset(self)
    ClearState(self)
end