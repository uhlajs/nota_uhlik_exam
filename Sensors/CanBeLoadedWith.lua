local sensorInfo = {
	name = "CanBeLoaded",
	desc = "Return true if `passangerID` can be loaded with `transporterID`.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local ValidUnitID = Spring.ValidUnitID
local GetUnitIsTransporting = Spring.GetUnitIsTransporting
local GetUnitTransporter = Spring.GetUnitTransporter

return function(passangerID, transporterID)
  return ValidUnitID(passangerID)
         and  
         ValidUnitID(transporterID)
         and 
         (GetUnitTransporter(passangerID) == nil or GetUnitTransporter(passangerID) == transporterID) -- Passanger already loaded
end