local sensorInfo = {
	name = "CheckTroopStatus",
	desc = "Check troops status.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(troops, status)
	for _, unitID in ipairs(troops) do
    if bb.troopsInfoMap[unitID].status ~= status then
      return false
    end
  end
  return true
end