local sensorInfo = {
	name = "QueryTroops",
	desc = "Query units according given query.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local ValidUnitID = Spring.ValidUnitID
local GetMyTeamID = Spring.GetMyTeamID
local GetTeamUnits = Spring.GetTeamUnits
local GetUnitDefID = Spring.GetUnitDefID

local function isInArray(element, array)
  if type(array) ~= "table" then return false end
  for _, e in ipairs(array) do
      if e == element then
          return true
      end
  end
  return false
end

--- Query units according given query.
--- Global bb variables:
---   `bb.troopsInfoMap` from `GenerateTroopsInfo.lua`
-- @param query table: query in format
-- query = {
--  status = unit status
--  group = group name
--  defsName = type of the unit
-- } 
-- @return [unitID]: array of filtered units.
return function(query)
  local troops = {}

  for unitID, unitInfo in pairs(bb.troopsInfoMap) do
    local valid = true
    for key, value in pairs(query) do
      if value ~= "*" and value ~= unitInfo[key] and not isInArray(value, unitInfo[key]) then
        valid = false
        break
      end
    end

    if valid then
      troops[#troops + 1] = unitID
    end
  end
  table.sort(troops, function(left, right) 
    return bb.troopsInfoMap[left].id < bb.troopsInfoMap[right].id
  end)
  return troops
end