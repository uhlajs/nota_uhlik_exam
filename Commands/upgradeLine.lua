-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Upgrade line",
        parameterDefs = {
            { 
                name = "lineName",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "GoodTop",
            },
            { 
                name = "upgradeLevel",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "1",
            },
        }
    }
end

local myTeamID = Spring.GetMyTeamID()

local function hasEnoughMetal(lineName, upgradeLevel)
    local resources = Spring.GetTeamResources(myTeamID, "metal")
    -- TODO: get real value of line upgrade
    local cost = upgradeLevel * 300
    local flag = resources >= cost + 5

    if flag then
        Spring.Echo("Buying line upgrade for '" .. lineName .. "' by "  .. upgradeLevel)
    else
        Spring.Echo("Not enought metal for upgrading '" .. lineName .. "' by " .. upgradeLevel .. " missing " .. cost - resources)
    end

    return flag
end

local function removeFromOrderList(lineName, upgradeLevel)
    local topOrder = bb.OrderList[1]

    -- Remove from order list
    if topOrder ~= nil and topOrder.defsName == "lineUpgrade" and topOrder.line == lineName and topOrder.count == upgradeLevel then
        table.remove(bb.OrderList, 1)
    end
end

function Run(self, units, parameter)
    if not hasEnoughMetal(parameter.lineName, parameter.upgradeLevel) then
        return FAILURE
    end

    removeFromOrderList(parameter.lineName, parameter.upgradeLevel)

    message.SendRules({
        subject = "swampdota_upgradeLine",
        data = {
            lineName = parameter.lineName,
            upgradeLevel = parameter.upgradeLevel,
        },
    })

    return SUCCESS
end


function Reset(self)
    return self
end